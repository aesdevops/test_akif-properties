FROM openjdk:11

WORKDIR /app

COPY target/*.jar app.jar

# COPY target/application.properties .

EXPOSE 8080

CMD [ "java","-jar", "app.jar"]
